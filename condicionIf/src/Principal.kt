/*
    Se ingresan por teclado 2 valores enteros. Si el primero es menor al segundo
    calcular la suma y la resta; sino calcular el producto y la división.
 */

fun main(parametro: Array<String>){
    print("Intro num 1: ")
    val valor1 = readLine()!!.toInt();

    print("Intro num 2: ")
    val  valor2 = readLine()!!.toInt();

    if (valor1<valor2){
        val suma = valor1+valor2
        val resta = valor1-valor2
        println("Suma: $suma")
        println("Resta: $resta")
    }else{
        val prod = valor1*valor2
        val divi = valor1/valor2
        println("Producto: $prod")
        println("División: $divi")
    }
}