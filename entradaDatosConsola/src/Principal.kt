fun main (argumento: Array<String>){
    print("Intro primer valor: ")
    val valor1 = readLine()!!.toInt()

    print("Intro segundo valor: ")
    val valor2 = readLine()!!.toInt()

    val suma = valor1 + valor2
    println("La suma de $valor1 + $valor2 = $suma")

    val prod = valor1 * valor2
    println("El producto de $valor1 * $valor2 = $prod")
}